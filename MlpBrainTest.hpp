#ifndef GP_MLPBRAINTEST_HPP
#define GP_MLPBRAINTEST_HPP

#include <iostream>
#include <sstream>
#include <cmath>
#include "Population.hpp"
#include "DynamicMatrixDimension.hpp"
#include "DynamicMatrix.hpp"
#include "MultiLayerPerceptron.hpp"

namespace gp{
    namespace test{

        namespace mlpBrain{
            /////////////////////////////////////////////////
            /// \brief Calculates and returns the "optimum" brain output for a given
            /// set of inputs, this is what the nueral network is trying to match
            /////////////////////////////////////////////////
            gp::DynamicMatrix<float> calcOptimumOutput(gp::DynamicMatrix<float> inputs){
                //the input vector is as follows:
                //0: input food smell left (amount of food to the left)
                //1: food smell right (amount of food to the right)
                //2: in sound back: sound of pursuing creature from behind - should go forward fast!
                //3: sound forward left: sound of pursing creature from front left, turn away right
                //4: sound forward right: sound of pursing creature from front left, turn away left
                //5: hunger -> if high, shouldnt breed!

                //output vector is as follows:
                //0: forward speed
                //1: turn left rate
                //2: turn right rate
                //3: breed flag

                gp::DynamicMatrix<float> out(1,4);

                if(inputs.at(0,5) < 0.5){ //if hunger is less than half
                    out.at(0,3) = 1; //..enable breeding
                } else {
                    out.at(0,3) = 0;
                }
                if(inputs.at(0,5) > 0.8){
                    //very high hunger, slow down
                    out.at(0,0) -= (inputs.at(0,5) - 0.8) * 2;
                }

                if(inputs.at(0,2) > 0.3){ //if sound behind, increase speed forward
                    out.at(0,0) += (inputs.at(0,2) - 0.3);
                }
                if(inputs.at(0,3) > 0.3){ //if sound front left, turn right
                    out.at(0,2) += inputs.at(0,3) - 0.3;
                    out.at(0,1) -= inputs.at(0,3) - 0.3;
                    out.at(0,0) += 0.2;
                }
                if(inputs.at(0,4) > 0.3){ //if sound front right, turn left
                    out.at(0,1) += inputs.at(0,4) - 0.3;
                    out.at(0,2) -= inputs.at(0,4) - 0.3;
                    out.at(0,0) += 0.2;
                }

                //1 means max food, 0 means no food
                float foodTotal = (inputs.at(0,0) + inputs.at(0,1))/2.0f;
                out.at(0,0) += (1 - foodTotal); //if total food is 0, move forward max speed, if food total is 1, move forward at speed 0

                //food left - food right
                //therefore, if +ve more food on left, turn left
                //if -ve more food on right, turn right
                float turnRate = inputs.at(0,0) - inputs.at(0,1);
                if(turnRate < 0){
                    // -= as it is -ve and we want to add the abs value
                    out.at(0,2) -= turnRate;
                } else{
                    out.at(0,1) += turnRate;
                }

                //ensure all output values are between 0 and 1
                for(int i = 0; i < 4; ++i){
                    if(out.at(0,i) < 0){out.at(0,i) = 0;}
                    if(out.at(0,i) > 1){out.at(0,i) = 1;}
                }

                return out;
            }

            /////////////////////////////////////////////////
            /// \brief formats a float to have at maximum precision p,
            /// trailing 0s will not be included (eg: 0.100 for p=4 will become 0.1)
            /////////////////////////////////////////////////
            std::string formatFloat(float f, int p){
                std::ostringstream strout ;
                strout << std::fixed;
                strout.precision(p);
                strout << f;
                std::string str = strout.str() ;
                size_t end = str.find_last_not_of( '0' ) + 1;
                return str.erase( end ) ;
            }

            void printCalcOptimumOutputRow(gp::DynamicMatrix<float> inputs){
                gp::DynamicMatrix<float> outputs = calcOptimumOutput(inputs);
                std::cout.width(6);
                std::cout << formatFloat(inputs.at(0,0), 4) << " ";
                std::cout.width(6);
                std::cout << formatFloat(inputs.at(0,1),4) << " ";
                std::cout.width(7);
                std::cout << formatFloat(inputs.at(0,2),4) << " ";
                std::cout.width(8);
                std::cout << formatFloat(inputs.at(0,3),4) << " ";
                std::cout.width(8);
                std::cout << formatFloat(inputs.at(0,4),4) << " ";
                std::cout.width(6);
                std::cout << formatFloat(inputs.at(0,5),4) << " || ";

                std::cout.width(5);
                std::cout << formatFloat(outputs.at(0,0),4) << " ";
                std::cout.width(6);
                std::cout << formatFloat(outputs.at(0,1),4) << " ";
                std::cout.width(6);
                std::cout << formatFloat(outputs.at(0,2),4) << " ";
                std::cout.width(5);
                std::cout << formatFloat(outputs.at(0,3),4) << " " << std::endl;

            }


            /////////////////////////////////////////////////
            /// \brief Tests and prints data on the optimum output function
            /////////////////////////////////////////////////
            void testCalcOptimumOutput(){
                std::cout << "Food-L Food-R Sound-B Sound-FL Sound-FR Hunger || Spd-F Turn-L Turn-R Breed" << std::endl;
                std::cout.unsetf(std::ios::floatfield);

                {
                    gp::DynamicMatrix<float> in(1,6);
                    in.at(0,0) = 0;
                    in.at(0,1) = 0;
                    in.at(0,2) = 0;
                    in.at(0,3) = 0;
                    in.at(0,4) = 0;
                    in.at(0,5) = 0;
                    printCalcOptimumOutputRow(in);
                }
                {
                    gp::DynamicMatrix<float> in(1,6);
                    in.at(0,0) = 1;
                    in.at(0,1) = 1;
                    in.at(0,2) = 1;
                    in.at(0,3) = 1;
                    in.at(0,4) = 1;
                    in.at(0,5) = 1;
                    printCalcOptimumOutputRow(in);
                }
                {
                    gp::DynamicMatrix<float> in(1,6);
                    in.at(0,0) = 1;
                    in.at(0,1) = 0.5;
                    in.at(0,2) = 0;
                    in.at(0,3) = 0;
                    in.at(0,4) = 0;
                    in.at(0,5) = 0.5;
                    printCalcOptimumOutputRow(in);
                }
                {
                    gp::DynamicMatrix<float> in(1,6);
                    in.at(0,0) = 1;
                    in.at(0,1) = 0.5;
                    in.at(0,2) = 0;
                    in.at(0,3) = 0.9;
                    in.at(0,4) = 0;
                    in.at(0,5) = 0.5;
                    printCalcOptimumOutputRow(in);
                }

            }

            float mlpTestCreatureBrainFitness(gp::MutliLayerPerceptron* mlp){


            }
        }



        void runMlpBrainTest(){
            gp::MutliLayerPerceptron mlp1({3,2});
            std::cout << mlp1.isValid() << std::endl;
            std::cout << mlp1.getInputNeuronCount() << std::endl;
            std::cout << mlp1.getOutputNeuronCount() << std::endl;
            std::cout << mlp1.getHiddenLayerCount() << std::endl;
            std::cout << mlp1.getLayerCount() << std::endl;
            gp::DynamicMatrix<float> inputValues(1,1);
            inputValues[0][0] = 1;
            std::cout << mlp1.evaluate(inputValues)[0][0] << std::endl;
        }

    }
}

#endif // GP_MLPBRAINTEST_HPP

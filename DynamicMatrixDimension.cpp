#include "DynamicMatrixDimension.hpp"

namespace gp{

    const DynamicMatrixDimension DynamicMatrixDimension::Invalid(0,0);

    DynamicMatrixDimension::DynamicMatrixDimension(unsigned short newRows, unsigned short newCols)
    : rows(newRows), columns(newCols){
        //emptyBody
    }

    DynamicMatrixDimension::DynamicMatrixDimension(const DynamicMatrixDimension& other)
    : DynamicMatrixDimension(other.rows, other.columns){
        //empty body
    }

    unsigned int DynamicMatrixDimension::getElementCount() const{
        return rows*columns;
    }

    bool DynamicMatrixDimension::operator==(const DynamicMatrixDimension& other) const{
        return this->rows == other.rows && this->columns == other.columns;
    }

    bool DynamicMatrixDimension::canPostMultiply(const DynamicMatrixDimension& other) const{
        //axb * cxd works if b == c and produces an axd matrix
        return this->columns == other.rows;
    }

    DynamicMatrixDimension DynamicMatrixDimension::getPostMultiplyDimension(const DynamicMatrixDimension& other) const{
        if(this->canPostMultiply(other)){
            return DynamicMatrixDimension(this->rows, other.columns);
        } else {
            return DynamicMatrixDimension(DynamicMatrixDimension::Invalid);
        }
    }
}

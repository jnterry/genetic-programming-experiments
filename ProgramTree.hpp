#ifndef GP_PROGRAMTREE_HPP
#define GP_PROGRAMTREE_HPP

#include <functional>
#include <vector>
#include <iostream>

namespace gp{

    /////////////////////////////////////////////////
    /// \brief Stores data about the structure of a ProgramTree
    /////////////////////////////////////////////////
    class ProgramTreeNode{
    public:
        ProgramTreeNode(unsigned int newArity, unsigned int newPrimativeId = 0)
        : arity(newArity), primativeId(newPrimativeId), children(nullptr){
            if(newArity != 0){
                children = new ProgramTreeNode*[newArity];
                for(int i = 0; i < newArity; ++i){
                    children[i] = nullptr;
                }
            }
        }

        /////////////////////////////////////////////////
        /// \brief Creates a deep copy of a tree node and all
        /// its children
        /////////////////////////////////////////////////
        ProgramTreeNode(const ProgramTreeNode& other)
        : ProgramTreeNode(other.arity, other.primativeId){
            for(int i = 0; i < other.arity; ++i){
                this->children[i] = new ProgramTreeNode(*other.children[i]);
            }
        }

        unsigned int arity;
        ProgramTreeNode** children;
        unsigned int primativeId;
    };

    class ProgramTreePrimativeSet{
    public:
        static const int MAX_ARITY = 5;

        friend class ProgramTreeGenerator;
        /////////////////////////////////////////////////
        /// \brief Stores data about any primative in a program tree
        /// \note Primatives with an arity of 0 are terminals, all other
        /// primatives are functions
        /////////////////////////////////////////////////
        struct Primative{
            Primative(std::string newName, unsigned int newArity, std::function<float (float*)> newFunction);
            unsigned int arity;
            std::string name;
            std::function<float (float*)> func;
        };

        /////////////////////////////////////////////////
        /// \brief Evaluates the given ProgramTreeNode (and hence all subnodes) and
        /// returns the result
        /////////////////////////////////////////////////
        float evaluateTree(ProgramTreeNode* tree);

        /////////////////////////////////////////////////
        /// \brief Returns a string holding a debug representation of the
        /// specified tree's structure
        /////////////////////////////////////////////////
        std::string debugTree(ProgramTreeNode* tree, unsigned int indent = 1);


        /////////////////////////////////////////////////
        /// \brief Returns a string holding the infix representation of
        /// the specified tree
        /////////////////////////////////////////////////
        std::string getInfixForTree(ProgramTreeNode* tree);

        /////////////////////////////////////////////////
        /// \brief Adds the specified primative to this PrimativeSet
        /////////////////////////////////////////////////
        void addPrimative(Primative primative);

        /////////////////////////////////////////////////
        /// \brief Returns the primative with the specified arity and id
        /////////////////////////////////////////////////
        Primative& getPrimative(unsigned int arity, unsigned int id);

    private:
        ///< Array of vectors, vector at index 0 stores all primatives with arity 0, etc
        std::vector<Primative> mPrimatives[MAX_ARITY];
    };

}

#endif // GP_PROGRAMTREE_HPP

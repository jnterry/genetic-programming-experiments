#include "ProgramTree.hpp"

#include <random>
#include <vector>

namespace{

}

namespace gp{
    ProgramTreePrimativeSet::Primative::Primative(std::string newName, unsigned int newArity, std::function<float (float*)> newFunction)
    : name(newName), arity(newArity), func(newFunction){
        //empty body
    }

    void ProgramTreePrimativeSet::addPrimative(Primative primative){
        this->mPrimatives[primative.arity].push_back(primative);
    }

    float ProgramTreePrimativeSet::evaluateTree(ProgramTreeNode* node){
        if(node->arity == 0){
            return this->mPrimatives[0][node->primativeId].func(nullptr);
        } else {
            //need to gather arguments for this node
            float* args = new float[node->arity];
            for(int i = 0; i < node->arity; ++i){
                args[i] = evaluateTree(node->children[i]);
            }
            return this->mPrimatives[node->arity][node->primativeId].func(args);
        }
    }

    std::string ProgramTreePrimativeSet::debugTree(ProgramTreeNode* tree, unsigned int indent){
        std::string result;
        result += this->mPrimatives[tree->arity][tree->primativeId].name + "\n";
        for(int i = 0; i < tree->arity; ++i){
            for(int j = 0; j < indent; ++j){
                result += " ";
            }
            result += debugTree(tree->children[i], indent+1);
        }
        return result;
    }

    ProgramTreePrimativeSet::Primative& ProgramTreePrimativeSet::getPrimative(unsigned int arity, unsigned int id){
        return this->mPrimatives[arity][id];
    }

    std::string ProgramTreePrimativeSet::getInfixForTree(ProgramTreeNode* tree){
        std::string result;
        if(tree->arity == 0){
            result += this->mPrimatives[tree->arity][tree->primativeId].name;
        } else if(tree->arity == 2){
            result += "(";
            result += getInfixForTree(tree->children[0]);
            result += this->mPrimatives[tree->arity][tree->primativeId].name;
            result += getInfixForTree(tree->children[1]);
            result += ")";
        } else {
            result += "UNKNOWN ARITY FOR INFIX";
        }
        return result;
    }


}

#ifndef GP_DYNAMICMATRIX_HPP
#define GP_DYNAMICMATRIX_HPP

#include "DynamicMatrixDimension.hpp"
#include <cstring>

namespace gp{



    /////////////////////////////////////////////////
    /// \brief A DynamicMatrix represents a matrix with an arbitary number
    /// of rows and columns, these values can be specified at runtime.
    /// This causes slower performance than using a compile time specified
    /// dimension as can be done with lie::math::Matrix however can be required
    /// for some uses.
    /////////////////////////////////////////////////
    template<typename TYPE_T>
    class DynamicMatrix{
    public:
        class _AccessHelper{
        public:
            friend DynamicMatrix;

            TYPE_T& operator[](unsigned int col){
                return this->mMatrix.mData[this->mMatrix.rowColToIndex(mRow, col)];
            }

            const TYPE_T& operator[](unsigned int col) const{
                return this->mMatrix.mData[this->mMatrix.rowColToIndex(mRow, col)];
            }
        private:
            /////////////////////////////////////////////////
            /// \brief Creates a new access helper for the specified matrix and column index
            /// \note Private, can only be called by friend class DynamicMatrix
            /////////////////////////////////////////////////
            _AccessHelper(unsigned int row, DynamicMatrix& matrix)
            : mRow(row), mMatrix(matrix){
                //empty body
            }


            ///< Which column in the DynamicMatrix is being accessed
            unsigned int mRow;

            ///< Reference to the matrix this AccessHelper is accessing
            DynamicMatrix& mMatrix;
        };

        friend _AccessHelper;

        ////////////////////////////////////////////////
        /// \brief Creates a new DynamicMatrix with the given dimension,
        /// all values are initialised to 0
        /////////////////////////////////////////////////
        DynamicMatrix(const DynamicMatrixDimension& dimension)
        : mDimension(dimension), mData(new TYPE_T[dimension.rows*dimension.columns]){
            for(int i = 0; i < dimension.getElementCount(); ++i){
                this->mData[i] = 0;
            }
        }

        /////////////////////////////////////////////////
        /// \brief Creates a new DynamicMatrix with the given number of rows
        /// and columns, all values are initialised to 0
        /////////////////////////////////////////////////
        DynamicMatrix(unsigned short rows, unsigned short cols)
        : DynamicMatrix(DynamicMatrixDimension(rows, cols)){
            //empty body
        }

        /////////////////////////////////////////////////
        /// \brief Creates a deep copy of the specified DynamicMatrix
        /////////////////////////////////////////////////
        DynamicMatrix(const DynamicMatrix& other)
        : mDimension(other.mDimension), mData(new TYPE_T[other.mDimension.rows*other.mDimension.columns]){
            for(int i = 0; i < other.mDimension.getElementCount(); ++i){
                this->mData[i] = other.mData[i];
            }
        }

        ~DynamicMatrix(){
            delete[] mData;
        }

        /////////////////////////////////////////////////
        /// \brief Multiplies this matrix by another, returning the result
        /////////////////////////////////////////////////
        DynamicMatrix operator*(const DynamicMatrix& other){
            if(this->mDimension.columns != other.mDimension.rows){
                //error, cant multiply
                return DynamicMatrix(0,0);
            }

            DynamicMatrix result(this->mDimension.getPostMultiplyDimension(other.mDimension));
            for(int resRow = 0; resRow < result.mDimension.rows; ++resRow){
                for(int resCol = 0; resCol < result.mDimension.columns; ++resCol){
                    result.mData[rowColToIndex(resRow, resCol)] = 0;
                    for(int i = 0; i < this->mDimension.columns; ++i){
                        result.mData[rowColToIndex(resRow, resCol)] += this->mData[this->rowColToIndex(resRow, i)] * other.mData[other.rowColToIndex(i, resCol)];
                    }
                }
            }
            return result;
        }

        const DynamicMatrixDimension& getDimension() const{
            return this->mDimension;
        }

        _AccessHelper operator[](unsigned short row){
            return _AccessHelper(row, *this);
        }

        TYPE_T& at(unsigned short row, unsigned short col){
            return this->mData[rowColToIndex(row,col)];
        }

        DynamicMatrix& operator=(const DynamicMatrix& other){
            delete[] mData;
            mDimension = other.mDimension;
            mData = new TYPE_T[other.mDimension.getElementCount()];
            memcpy(this->mData, other.mData, sizeof(TYPE_T)*other.mDimension.getElementCount());
        }
    private:
        unsigned int rowColToIndex(unsigned short row, unsigned short col) const{
            return (row*this->mDimension.columns) + col;
        }

        ///< The Dimension data for this matrix
        ///< Not const as need operator =
        DynamicMatrixDimension mDimension;

        ///< Pointer to a 1d array of TYPE_T containg the data for this matrix
        ///< Array's length is equal to mDimension.getElementCount()
        TYPE_T* mData;
    };

}

#endif // GP_DYNAMICMATRIX_HPP

#include "ProgramTreeGenerator.hpp"
#include "ProgramTree.hpp"

#include <ctime>
#include <iostream>

namespace gp{

    ProgramTreeGenerator::ProgramTreeGenerator(ProgramTreePrimativeSet& ps) : mPrimSet(ps), mRandPercent(0,100){
        if(ps.mPrimatives[0].size() == 0){
            throw std::runtime_error("Attempted to construct a ProgramTreeGenerator but the primative set did not contain any terminals!");
        }

        time_t t;
        time(&t);
        mRGen.seed(t);

        for(int i = 0; i < ProgramTreePrimativeSet::MAX_ARITY; ++i){
            mRandPrimativeId[i] = std::uniform_int_distribution<int>(0, ps.mPrimatives[i].size()-1);
        }

        //////////////////////////////////
        //Set up function arity randomizer
        bool activeArities[ProgramTreePrimativeSet::MAX_ARITY-1];//dont include 0 arity
        unsigned int activeCount = 0;
        for(int i = 0; i < ProgramTreePrimativeSet::MAX_ARITY-1; ++i){
            if((ps.mPrimatives[i+1].size() != 0)){
                activeArities[i] = true;
                ++activeCount;
            } else {
                activeArities[i] = false;
            }
        }

        this->mValidFuncArityCount = activeCount;
        //std::cout << "Valid func arity count: " << this->mValidFuncArityCount << std::endl;

        mRndToFuncArityMap = new unsigned int[this->mValidFuncArityCount];
        for(int i = 1, c = 0; i < ProgramTreePrimativeSet::MAX_ARITY-1; ++i){
            if(activeArities[i]){
                mRndToFuncArityMap[c] = i+1;
                //std::cout << "RndToFuncArityMap[" << c << "]: " << i+1 << std::endl;
                ++c;
            }
        }
        //lets say there are functions with arity 0,1,2,4
        //mRndToFuncArityMap will now be:
        //[0] = 1
        //[1] = 2
        //[2] = 4
        //hence we can map a random number between 0 and this->mValidFuncArityCount to the arity
        //of a function
        //std::cout << "Active count -1 : " << activeCount - 1 << std::endl;
        mRandFunctionArity = std::uniform_int_distribution<int>(0, activeCount-1);

    }

    ProgramTreeNode* ProgramTreeGenerator::randomFunction(){
        /*std::cout << "Getting random function..." << std::endl;
        unsigned int index = mRandFunctionArity(mRGen);
        std::cout << "  index: " << index << std::endl;
        unsigned int arity = this->mRndToFuncArityMap[index];
        std::cout << "  arity: " << index << std::endl;
        return randomPrimativeWithArity(arity);*/
        return randomPrimativeWithArity(this->mRndToFuncArityMap[mRandFunctionArity(mRGen)]);
    }

    ProgramTreeNode* ProgramTreeGenerator::randomPrimativeWithArity(unsigned int arity){
        return new ProgramTreeNode(arity, mRandPrimativeId[arity](mRGen));
    }

    ProgramTreeNode* ProgramTreeGenerator::generateRandomTree(){
        ProgramTreeNode* root = randomFunction();
        std::vector<ProgramTreeNode*> unfinished;
        unfinished.push_back(root);

        int createdNodes = 0;
        while(unfinished.size() > 0){
            ProgramTreeNode* cur = unfinished.back();
            unfinished.pop_back();

            //std::cout << "unfinished size: " << unfinished.size() << ", created nodes: " << createdNodes << std::endl;

            for(int i = 0; i < cur->arity; ++i){
                //std::cout << "cur arity: " << cur->arity << std::endl;
                int choice = mRandPercent(mRGen);
                //std::cout << "choice: " << choice << std::endl;
                if(choice > 45 + createdNodes*5){
                    //add func
                    cur->children[i] = randomFunction();
                    unfinished.push_back(cur->children[i]);
                    createdNodes++;
                    //std::cout << "created function" << std::endl;
                } else {
                    //add terminator
                    cur->children[i] = randomPrimativeWithArity(0);
                    createdNodes++;
                    //std::cout << "created terminal" << std::endl;
                }
            }
        }
        return root;
    }

    ProgramTreeNode* ProgramTreeGenerator::getRandomDecendent(ProgramTreeNode* node, unsigned int goDownChance){
        if(node->arity == 0){
            return node;
        }

        if(mRandPercent(mRGen) < goDownChance){
            std::uniform_int_distribution<int> randChild(0, node->arity-1);
            return getRandomDecendent(node->children[randChild(mRGen)], goDownChance);
        } else {
            return node;
        }
    }

    std::pair<ProgramTreeNode*, ProgramTreeNode*> ProgramTreeGenerator::breedTrees(ProgramTreeNode* p1, ProgramTreeNode* p2){
        ProgramTreeNode* child1 = new ProgramTreeNode(*p1);
        ProgramTreeNode* child2 = new ProgramTreeNode(*p2);

        ProgramTreeNode* cross1;
        ProgramTreeNode* cross2;
        do{
            cross1 = getRandomDecendent(child1,70);
        }while(cross1->arity == 0); //keep going until we get a non leaf node
        do{
            cross2 = getRandomDecendent(child2,70);
        }while(cross2->arity == 0);
        std::uniform_int_distribution<int> r1(0, cross1->arity-1);
        unsigned int i1 = r1(mRGen);
        std::uniform_int_distribution<int> r2(0, cross2->arity-1);
        unsigned int i2 = r2(mRGen);

        ProgramTreeNode* temp = child1->children[i1];
        child1->children[i1] = child2->children[i2];
        child2->children[i2] = temp;

        return std::make_pair(child1, child2);
    }

    void ProgramTreeGenerator::mutateTree(ProgramTreeNode* tree, unsigned int mutatePercent){
        if(mRandPercent(mRGen) < mutatePercent){
            tree->primativeId = mRandPrimativeId[tree->arity](mRGen);
        }
        for(int i = 0; i < tree->arity; ++i){
            mutateTree(tree->children[i], mutatePercent);
        }
    }
}

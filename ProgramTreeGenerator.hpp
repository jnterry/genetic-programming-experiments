#ifndef GP_PROGRAMTREEGENERATOR_HPP
#define GP_PROGRAMTREEGENERATOR_HPP

#include "ProgramTree.hpp"
#include <random>

namespace gp{

    class ProgramTreeGenerator{
    public:
        /////////////////////////////////////////////////
        /// \brief Constructs a new ProgramTreeGenerator for the given primative set
        /// \note If the primative set is modified after adding it here
        /// the changes will nnot be reflected by the state of this generator, you
        /// must make a new one!
        /////////////////////////////////////////////////
        ProgramTreeGenerator(ProgramTreePrimativeSet& ps);

        ProgramTreeNode* generateRandomTree();

        /////////////////////////////////////////////////
        /// \brief Breed the two passed in trees by selecting a cross over point
        /// in each and swapping the sub trees, returns two new children in an std::pair,
        /// leaves original trees unchanged
        /////////////////////////////////////////////////
        std::pair<ProgramTreeNode*, ProgramTreeNode*> breedTrees(ProgramTreeNode* p1, ProgramTreeNode* p2);

        /////////////////////////////////////////////////
        /// \brief Mutates the given tree by randomly replacing nodes with
        /// nodes of equal arity
        /// Every decendent of the specified node is considered (as well as the specified node)
        /// and will be mutated "mutatePercent" percent of the time
        /// \param mutatePercent
        /////////////////////////////////////////////////
        void mutateTree(ProgramTreeNode* tree, unsigned int mutatePercent);

    private:
        std::default_random_engine mRGen;

        std::uniform_int_distribution<int> mRandPercent;

        std::uniform_int_distribution<int> mRandFunctionArity;
        unsigned int* mRndToFuncArityMap; ///< pointer to array
        unsigned int mValidFuncArityCount; ///< length of above array

        std::uniform_int_distribution<int> mRandPrimativeId[ProgramTreePrimativeSet::MAX_ARITY];

        ProgramTreePrimativeSet& mPrimSet;


        /////////////////////////////////////////////////
        /// \brief Returns a new ProgramTreeNode representing any random function
        /////////////////////////////////////////////////
        ProgramTreeNode* randomFunction();

        /////////////////////////////////////////////////
        /// \brief Returns a new ProgramTreeNode reprenting any random
        /// primative with the given arity
        /////////////////////////////////////////////////
        ProgramTreeNode* randomPrimativeWithArity(unsigned int arity);

        /////////////////////////////////////////////////
        /// \brief Finds and returns a random decendent of the specified node
        /////////////////////////////////////////////////
        ProgramTreeNode* getRandomDecendent(ProgramTreeNode* node, unsigned int goDownChance);
    };

}

#endif // GP_PROGRAMTREEGENERATOR_HPP

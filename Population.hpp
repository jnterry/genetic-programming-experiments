#ifndef GP_POPULATION_HPP
#define GP_POPULATION_HPP

#include <random>
#include <ctime>
#include <algorithm>
#include <iostream>

namespace gp{
    template<typename MEM_T>
    class Population{
    public:
        Population(){
            time_t t;
            time(&t);
            mRGen.seed(t);
        }
        struct Member{
            MEM_T* member;
            float fitness;
        };

        /////////////////////////////////////////////////
        /// \brief Returns the member at the specified index, index 0
        /// has the most fit member, etc
        /////////////////////////////////////////////////
        Member getMember(unsigned int index){
            return mMembers[index];
        }

        /////////////////////////////////////////////////
        /// \brief Returns the number of members in the population
        /////////////////////////////////////////////////
        unsigned int getMemberCount(){
            return mMembers.size();
        }

        /////////////////////////////////////////////////
        /// \brief Adds a new member to the population, calculates its fitness and
        /// resorts the population
        /////////////////////////////////////////////////
        unsigned int addMember(MEM_T* mem){
            Member m;
            m.member = mem;
            m.fitness = mFitnessFunction(mem);
            this->mMembers.push_back(m);
            this->sortMembers();
        }

        /////////////////////////////////////////////////
        /// \brief Sorts the members so that the one with the highest
        /// fitness is at index 0, etc
        /////////////////////////////////////////////////
        void sortMembers(){
            std::sort(mMembers.begin(), mMembers.end(), [](Member m1, Member m2) {return m1.fitness > m2.fitness;});
        }

        /////////////////////////////////////////////////
        /// \brief Gets a random member from the population.
        /// There is an exponentially decreasing chance of getting the less fit members
        /// eg:
        /// \code
        /// [0] **************** //most fit, high chance
        /// [1] ***********
        /// [2] ****
        /// [3] **
        /// [4] *
        /// \endcode
        /////////////////////////////////////////////////
        Member& getRandomMemberExpon(){
            std::exponential_distribution<float> parentChooser(8);
            unsigned int index = 0;
            do{
               index = parentChooser(mRGen) * (this->mMembers.size() / 1.3);
            }while(! (index < this->mMembers.size()));
            //keep going until index is within range, this should be fairly quick if not in the first iteration
            return this->mMembers[index];
        }

        /////////////////////////////////////////////////
        /// \brief Culls the population down to the specified size, deleting the least fit members
        /// of the population
        /////////////////////////////////////////////////
        void cullPopulation(unsigned int newSize){
            if(newSize >= this->mMembers.size()){
                //dont need to cull anything
                return;
            }

            unsigned int delta = this->mMembers.size() - newSize;
            for(int i = 0; i < delta; ++i){
                this->mMembers.pop_back();
            }
            return;
        }

        void setFitnessFunction(std::function<float(MEM_T*)> func){
            mFitnessFunction = func;
        }

    private:
        std::vector<Member> mMembers;

        ///< Function used to determine the fitness of a member of the population
        ///< Takes a pointer to the member and should return a float, higher floats
        ///< are considered to be fitter.
        std::function<float(MEM_T*)> mFitnessFunction;

        std::default_random_engine mRGen;
    };
}

#endif // GP_PPOPULATION_HPP

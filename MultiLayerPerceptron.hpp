#ifndef GP_MULTILAYERPERCEPTRON_HPP
#define GP_MULTILAYERPERCEPTRON_HPP

#include <vector>
#include <initializer_list>
#include "DynamicMatrix.hpp"

namespace gp{

    /////////////////////////////////////////////////
    /// \brief Class representing a multilayer perceptron
    /// nueral network
    /////////////////////////////////////////////////
    class MutliLayerPerceptron{
    public:
        /////////////////////////////////////////////////
        /// \brief Creates a new MultiLayerPerceptron with the specified structure.
        /// The first element of the structure list will be the number of inputs this MLP
        /// takes, the last element is the number of outputs.
        /// All extra elements specify the number of nuerons in hidden layers.
        /// eg {5,8,5,3} specifies a MLP with 5 input nuerons, 2 hidden layers with 8 and then 5
        /// nuerons respectivly and 3 output nuerons.
        /// The list MUST contain atleast 2 elements to specify the input and output count
        ///
        /// This will also setup empty weight matricies for the nueral network, the dimensions will
        /// be correct but all values will be set to 0
        ///
        /// \param structure an std::initializer_list containing the data
        /////////////////////////////////////////////////
        MutliLayerPerceptron(std::initializer_list<unsigned short> structure)
        : MutliLayerPerceptron(structure.size(), structure.begin()){
            //empty body
        }

        /////////////////////////////////////////////////
        /// \brief Creates a new MutliLayerPerceptron with the specified structure.
        /// The first element of the structure list will be the number of inputs this MLP
        /// takes, the last element is the number of outputs.
        /// All extra elements specify the number of nuerons in hidden layers.
        /// eg {5,8,5,3} specifies a MLP with 5 input nuerons, 2 hidden layers with 8 and then 5
        /// nuerons respectivly and 3 output nuerons.
        /// The list MUST contain atleast 2 elements to specify the input and output count
        ///
        /// This will also setup empty weight matricies for the nueral network, the dimensions will
        /// be correct but all values will be set to 0
        ///
        /// \param layerCount Number of layers (ie: length of structure array), must be atleast 2
        /// \param structure pointer to the data to read
        /////////////////////////////////////////////////
        MutliLayerPerceptron(unsigned short layerCount, const unsigned short* structure);

        /////////////////////////////////////////////////
        /// \brief Evaluates the MLP using the specified DynamicMatrix as the input vector,
        /// returns another DynamicMatrix<float> with the results of the MLP
        /////////////////////////////////////////////////
        DynamicMatrix<float> evaluate(DynamicMatrix<float> inputs);


        /////////////////////////////////////////////////
        /// \brief Returns true if this MLP is valid
        /// In order to be valid it must include 1 or more input nueron and 1 or more output nueron.
        /// The weight matricies linking layers must have dimensions which allow them to be multiplied
        /// Eg, if hidden layer 0 is produced from a matrix that makes 5 nuerons but then the next matrix
        /// assumes there are 6 nuerons there is an error.
        /////////////////////////////////////////////////
        bool isValid();

        /////////////////////////////////////////////////
        /// \brief Returns the number of input neurons this MLP has
        /////////////////////////////////////////////////
        unsigned short getInputNeuronCount();

        /////////////////////////////////////////////////
        /// \brief Returns the number of output neurons this MLP has
        /////////////////////////////////////////////////
        unsigned short getOutputNeuronCount();

        /////////////////////////////////////////////////
        /// \brief Returns the number of hidden layers in this MLP
        /////////////////////////////////////////////////
        unsigned short getHiddenLayerCount();

        /////////////////////////////////////////////////
        /// \brief Returns the total number of layers in this MLP including
        /// the input and output layer, this is hiddenLayerCount+2
        /////////////////////////////////////////////////
        unsigned short getLayerCount();
    private:
        ///< Pointer to array containing the number of nuerons in each layer of the MLP
        ///< mStructure[0] is the number of inputs mStructure[mLayerCount-1] is the number of outputs
        unsigned short* mStructure;

        ///< The number of layers in this MLP including the input and output layer, therefore it must
        ///< be atleast 2!
        unsigned short mLayerCount;

        ///< Vector of dynamic matricies which represent the wieght between nuerons in each layer
        ///< Note, each layer has 1 extra "bias" nueron which always has a value of 1, this is nueron
        ///< 0 in each layer
        std::vector<DynamicMatrix<float>> mWeightMatricies;

    };

}

#endif // GP_MULTILAYERPERCEPTRON_HPP

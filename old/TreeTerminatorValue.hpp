#ifndef TREETERMINATORVALUE_HPP
#define TREETERMINATORVALUE_HPP

#include "TreeEvolver.hpp"

namespace gp{
    class TreeTerminatorValue : public TreeTerminator{
    public:
        TreeTerminatorValue(float val = 0, std::string dstr = "0") : TreeTerminator(dstr), value(val){}
        TreeTerminatorValue(const TreeTerminatorValue& other) : TreeTerminatorValue(other.value, other.getDebugString()){}

        TreePrimative* clone(){
            return new TreeTerminatorValue(*this);
        }

        float getValue(){return value;}
        float value;
    };
}

#endif // TREETERMINATORVALUE_HPP

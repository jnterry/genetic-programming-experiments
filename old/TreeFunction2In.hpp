#ifndef TREEFUNCTION2IN_HPP
#define TREEFUNCTION2IN_HPP

#include <functional>

namespace gp{
    class TreeFunction2In : public TreePrimative{
    public:
        TreeFunction2In(std::function<float(float, float)> func, std::string debugStr) : TreePrimative(2, debugStr), mFunction(func){}
        TreeFunction2In(const TreeFunction2In& other) : TreeFunction2In(other.mFunction, other.getDebugString()){}

        TreePrimative* clone(){
            return new TreeFunction2In(*this);
        }

        float getValue(){
            /*if(mChildren[0] == nullptr){
                std::cout << "TreeFunction2In Child0 is nullptr" <<  std::endl;
            }
            if(mChildren[1] == nullptr){
                std::cout << "TreeFunction2In Child1 is nullptr" <<  std::endl;
            }*/
            return mFunction(mChildren[0]->getValue(), mChildren[1]->getValue());
        }
    private:
        std::function<float(float,float)> mFunction;
    };
}

#endif // TREEFUNCTION2IN_HPP

#include "TreeEvolver.hpp"

#include <random>
#include <iostream>
#include <ctime>
#include <algorithm>

namespace gp{

    TreeEvolver::TreeEvolver(){
        time_t  t;
        time(&t);
        mRGen.seed(t);
    }
    TreePrimative* TreeEvolver::generateRandomTree(){
        TreePrimative* root;

        std::uniform_int_distribution<int> randPercent(0, 100);
        std::vector<TreePrimative*> unfinished;

        root = getRandomFunction()->clone();
        unfinished.push_back(root);

        int createdNodes = 0;
        while(unfinished.size() > 0){
            TreePrimative* cur = unfinished.back();
            unfinished.pop_back();

            //std::cout << "unfinished size: " << unfinished.size() << ", created nodes: " << createdNodes << std::endl;

            for(int i = 0; i < cur->getArity(); ++i){
                int choice = randPercent(mRGen);
                //std::cout << "choice: " << choice << std::endl;
                if(choice > 45 + createdNodes*5){
                    //add func
                    cur->mChildren[i] = getRandomFunction()->clone();
                    unfinished.push_back(cur->mChildren[i]);
                    createdNodes++;
                } else {
                    //add terminator
                    cur->mChildren[i] = getRandomTerminator();
                    createdNodes++;
                }
            }
        }
        return root;
    }

    void TreeEvolver::initPopulation(int count){
        for(int i = 0; i < count; ++i){
            PopulationMember mem;
            mem.tree = this->generateRandomTree();
            mem.fitness = this->fitnessFunction(*this, mem.tree);
            this->population.push_back(mem);
        }
        this->sortPopulation();
    }

    void TreeEvolver::mutatePopulation(int maxChildren){
        std::exponential_distribution<float> parentChooser(2);
        unsigned int oldChildCount = this->population.size();

        for(int i = 0; i < maxChildren/2; ++i){
            PopulationMember mem1;
            PopulationMember mem2;

            mem1.tree = this->population[parentChooser(mRGen)].tree->cloneTree();
            mem2.tree = this->population[parentChooser(mRGen)].tree->cloneTree();

            /*std::cout << "Parent 1: " << std::endl;
            std::cout << mem1.tree->getTreeDebugString() << std::endl;
            std::cout << "Parent 2: " << std::endl;
            std::cout << mem2.tree->getTreeDebugString() << std::endl;*/


            //select the two cross over points
            std::uniform_int_distribution<int> randPercent(0, 100);
            TreePrimative** cross1 = getRandomTreePrimative(mem1.tree, 65);
            TreePrimative** cross2 = getRandomTreePrimative(mem2.tree, 65);

            //do the cross over
            TreePrimative* temp = *cross1;
            *cross1 = *cross2;
            *cross2 = *cross1;

            ///////////////////////////////////////
            //MUTATE
            while(randPercent(mRGen) < 40){
                TreePrimative** node = getRandomTreePrimative(mem1.tree, 90);
                TreePrimative* oldNode = *node;
                if(oldNode->getArity() == 0){
                    //then replace with another terminal
                    *node = getRandomTerminator();
                } else {
                    *node = getRandomFunction()->clone();
                    for(int i = 0; i < oldNode->getArity(); ++i){
                        (*node)->mChildren[i] = oldNode->mChildren[i];
                    }
                }
            }

            mem1.fitness = this->fitnessFunction(*this, mem1.tree);
            mem2.fitness = this->fitnessFunction(*this, mem2.tree);
            this->population.push_back(mem1);
            this->population.push_back(mem2);
        }
        this->sortPopulation();
    }

    void TreeEvolver::cullPopulation(unsigned int newSize){
        this->sortPopulation(); //just incase
        if(newSize > this->population.size()){
            return;
        }
        unsigned int delta = this->population.size() - newSize;
        for(int i = 0; i < delta; ++i){
            this->population.pop_back();
        }
    }

    void TreeEvolver::sortPopulation(){
        std::sort(this->population.begin(), this->population.end(), [](PopulationMember pm1, PopulationMember pm2){return pm1.fitness < pm2.fitness;});
    }

    TreePrimative** TreeEvolver::getRandomTreePrimative(TreePrimative* root, unsigned int goDownTreeChance){
        TreePrimative* r = root; //dont want to modify passed in root

        TreePrimative** result = &r;
        std::uniform_int_distribution<int> randPercent(0, 100);
        while(randPercent(mRGen) < goDownTreeChance){
            if((*result)->getArity() == 0){
                break;
            } else {
                std::uniform_int_distribution<int> randChild(0, (*result)->getArity()-1);
                result = & ((*result)->mChildren[randChild(mRGen)]);
            }
        }
        return result;
    }

    //returns pointer to random terminal owned by this TreeEvolver
    TreePrimative* TreeEvolver::getRandomTerminator(){
        std::uniform_int_distribution<int> randTerm(0, this->terminators.size()-1);
        return this->terminators[randTerm(mRGen)];
    }

    //returns pointer to random function owned by this TreeEvolver
    TreePrimative* TreeEvolver::getRandomFunction(){
        std::uniform_int_distribution<int> randFunc(0, this->functions.size()-1);
        return this->functions[randFunc(mRGen)];
    }
}



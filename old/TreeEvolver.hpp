#ifndef TREEEVOLVE_HPP
#define TREEEVOLVE_HPP

#include <vector>
#include <string>
#include <functional>
#include <random>

namespace gp{

    class TreePrimative{
    public:
        TreePrimative(unsigned int arity, std::string dstr) : mArity(arity), mDebugString(dstr){
            if(arity == 0){
                mChildren = nullptr;
            } else {
                mChildren = new TreePrimative*[arity];
                for(int i = 0; i < arity; ++i){
                    mChildren[i] = nullptr;
                }
            }
        }
        virtual float getValue() = 0;

        std::string getDebugString() const {return mDebugString;}

        //gets debug string for entire tree
        std::string getTreeDebugString(int indentLevel = 1){
            std::string result = "";
            result += this->mDebugString + "\n";
            for(int i = 0; i < this->mArity; ++i){
                for(int j = 0; j < indentLevel; ++j){
                    result += " ";
                }
                result += this->mChildren[i]->getTreeDebugString(indentLevel+1);
            }
            return result;
        }


        unsigned int getArity(){return mArity;}

        //creates and returns new instance of TreePrimative that is copy of this one
        //polymorphically creates instance of correct type
        virtual TreePrimative* clone() = 0;

        TreePrimative* cloneTree(){
            if(this->mArity == 0){
                //dont want to clone terminators
                return this;
            } else {
                TreePrimative* root = this->clone();
                for(int i = 0; i < this->mArity; ++i){
                    root->mChildren[i] = this->mChildren[i]->cloneTree();
                }
                return root;
            }

        }
    //private:
        TreePrimative** mChildren;
        unsigned int mArity;
        std::string mDebugString;
    };

    class TreeTerminator : public TreePrimative{
    public:
        TreeTerminator(std::string dstr) : TreePrimative(0, dstr){}
    };


    class TreeEvolver{
    public:
        TreeEvolver();

        struct PopulationMember{
            TreePrimative* tree;
            float fitness;
        };

        //generates set number of random trees and adds them to the tree evolvers
        //population vector
        void initPopulation(int count);

        void mutatePopulation(int maxChildren);

        //Generates new random tree from the terminators and functions in this TreeEvolver
        TreePrimative* generateRandomTree();

        //culls the population down to the specified size, keeps the best members of the population
        void cullPopulation(unsigned int newSize);

        std::vector<TreeTerminator*> terminators;
        std::vector<TreePrimative*> functions;
        std::vector<PopulationMember> population;
        std::function<float (TreeEvolver&, TreePrimative*)> fitnessFunction;
    private:
        std::default_random_engine mRGen;

        //sorts population so most fit member is at index 0, etc
        void sortPopulation();

        //returns pointer to random terminal owned by this TreeEvolver
        TreePrimative* getRandomTerminator();

        //returns pointer to random function owned by this TreeEvolver
        TreePrimative* getRandomFunction();


        //selets a random tree primative and returns a pointer to the pointer to it in its parent
        TreePrimative** getRandomTreePrimative(TreePrimative* root, unsigned int goDownTreeChance);
    };
}




#endif // TREEEVOLVE_HPP

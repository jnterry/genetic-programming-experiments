#include "QuadraticProgramTreeTest.hpp"
#include "MlpBrainTest.hpp"


int main(int argc, const char* argv){

    //gp::test::runQuadProgTreeTest();

    //gp::test::runMlpBrainTest();

    gp::test::mlpBrain::testCalcOptimumOutput();

    std::cout << "Terminating program..." << std::endl;
    return 0;
}

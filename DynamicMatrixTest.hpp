#ifndef GP_DYNAMICMATRIXTEST_HPP
#define GP_DYNAMICMATRIXTEST_HPP

namespace gp{
    namespace test{

        void dynamicMatrixTest(){
            gp::DynamicMatrixDimension matd1(1,3);
            gp::DynamicMatrixDimension matd2(3,4);
            gp::DynamicMatrixDimension matd3(4,3);

            std::cout << "mat1 * mat2: " << matd1.canPostMultiply(matd2) << std::endl;
            std::cout << "mat2 * mat1: " << matd2.canPostMultiply(matd1) << std::endl;
            std::cout << "mat1 * mat3: " << matd1.canPostMultiply(matd3) << std::endl;
            std::cout << "mat3 * mat1: " << matd3.canPostMultiply(matd1) << std::endl;

            gp::DynamicMatrixDimension matd4 = matd1.getPostMultiplyDimension(matd2);
            std::cout << "result: " << matd4.rows << ", " << matd4.columns << std::endl;

            gp::DynamicMatrix<float> mat1(1,3);
            mat1.at(0,0) = 1;
            mat1.at(0,1) = 2;
            mat1.at(0,2) = 3;

            gp::DynamicMatrix<float> mat2(3,4);
            mat2.at(0,0) = 1;
            mat2.at(0,1) = 2;
            mat2.at(0,2) = 3;
            mat2.at(0,3) = 4;
            mat2.at(1,0) = 5;
            mat2.at(1,1) = 6;
            mat2.at(1,2) = 7;
            mat2.at(1,3) = 8;
            mat2.at(2,0) = 9;
            mat2.at(2,1) = 10;
            mat2.at(2,2) = 11;
            mat2.at(2,3) = 12;

            gp::DynamicMatrix<float> result = mat1 * mat2;
            std::cout << "result dimension: " << result.getDimension().rows << "x" << result.getDimension().columns << std::endl;
            std::cout << result.at(0,0) << ", " << result.at(0,1) << ", " << result.at(0,2) << ", " << result.at(0,3) << std::endl;
        }

    }
}

#endif // GP_DYNAMICMATRIXTEST_HPP

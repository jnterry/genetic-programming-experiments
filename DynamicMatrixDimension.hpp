#ifndef GP_MATRIXDIMENSION_HPP
#define GP_MATRIXDIMENSION_HPP

namespace gp{
    /////////////////////////////////////////////////
    /// \brief Holds data about the dimension of a matrix (ie: number of
    /// rows and columns). These values can be specified and changed at run time.
    /// Also provides functions to assess whether dimensions are the same, can be
    /// multipled, etc
    /////////////////////////////////////////////////
    struct DynamicMatrixDimension{
        ///< An Invaid DynamicMatrixDimension with 0 rows and 0 columns
        static const DynamicMatrixDimension Invalid;

        ///< Number of rows
        unsigned short rows;

        ///< Number of columns
        unsigned short columns;

        /////////////////////////////////////////////////
        /// \brief Creates a new DynamicMatrixDimension struct with the specified number
        /// of rows and columns
        /////////////////////////////////////////////////
        DynamicMatrixDimension(unsigned short newRows, unsigned short newCols);

        /////////////////////////////////////////////////
        /// \brief Creates a deep copy of the specified DynamicMatrixDimension
        /////////////////////////////////////////////////
        DynamicMatrixDimension(const DynamicMatrixDimension& other);

        /////////////////////////////////////////////////
        /// \brief Returns the total number of elements in a matrix of this dimension
        /// (ie: rows * columns)
        /////////////////////////////////////////////////
        unsigned int getElementCount() const;

        /////////////////////////////////////////////////
        /// \brief Returns true if two DynamicMatrixDimensions are exactly equal,
        /// (ie: either number of rows and columns are the same in both instances). Else
        /// returns false
        /////////////////////////////////////////////////
        bool operator==(const DynamicMatrixDimension& other) const;

        /////////////////////////////////////////////////
        /// \brief Returns true if two DynamicMatrixDimensions are not exactly equal,
        /// (ie: either the number of rows or columns or both differ). Else
        /// returns false
        /////////////////////////////////////////////////
        bool operator!=(const DynamicMatrixDimension& other){return !this->operator==(other);}

        /////////////////////////////////////////////////
        /// \brief Returns true if it is possible to multiple a matrix of this dimension
        /// with another of the specified dimension
        /// eg: THIS * OTHER, not OTHER * THIS
        /////////////////////////////////////////////////
        bool canPostMultiply(const DynamicMatrixDimension& other) const;


        /////////////////////////////////////////////////
        /// \brief Returns the dimension of the matrix that would be produced by multipling
        /// a matrix of this dimension with the specified one after it,
        /// eg: THIS * OTHER, not OTHER * THIS
        ///
        /// \note If it is not possible to do the multiplication a copy of DynamicMatrixDimension::Invalid is returned
        /////////////////////////////////////////////////
        DynamicMatrixDimension getPostMultiplyDimension(const DynamicMatrixDimension& other) const;
    };
}

#endif // GP_MATRIXDIMENSION_HPP

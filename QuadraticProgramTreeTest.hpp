#ifndef GP_QUADRATICPROGRAMTREETEST_HPP
#define GP_QUADRATICPROGRAMTREETEST_HPP

#include <iostream>
#include <sstream>
#include <cmath>
#include "ProgramTree.hpp"
#include "ProgramTreeGenerator.hpp"
#include "Population.hpp"

namespace gp{
    namespace test{

        float quadraticEquation(float x){
            return 3*x*x - 5*x + 10;
        }

        void runQuadProgTreeTest(){
            std::cout << "Registering Tree Primatives..." << std::endl;
            gp::ProgramTreePrimativeSet ptps;
            ptps.addPrimative(gp::ProgramTreePrimativeSet::Primative("+",2,[](float* args){return args[0] + args[1];}));
            ptps.addPrimative(gp::ProgramTreePrimativeSet::Primative("-",2,[](float* args){return args[0] - args[1];}));
            ptps.addPrimative(gp::ProgramTreePrimativeSet::Primative("*",2,[](float* args){return args[0] * args[1];}));
            ptps.addPrimative(gp::ProgramTreePrimativeSet::Primative("/",2,[](float* args){return args[0] / args[1];}));

            ptps.addPrimative(gp::ProgramTreePrimativeSet::Primative("x",0,[](float* args){return 0;}));
            for(float f = -10; f <= 10; ++f){
                std::stringstream ss;
                ss << f;
                ptps.addPrimative(gp::ProgramTreePrimativeSet::Primative(ss.str(),0,[f](float* args){return f;}));
            }

            gp::ProgramTreeGenerator ptgen(ptps);

            gp::Population<gp::ProgramTreeNode> treePop;

            treePop.setFitnessFunction([&ptps](gp::ProgramTreeNode* tree){
                                            float fitness;
                                            for(float f = -30; f < 30; ++f){
                                                ptps.getPrimative(0,0).func = [f](float* args){return f;};
                                                float delta = quadraticEquation(f) - ptps.evaluateTree(tree);
                                                fitness -= fabs(delta);
                                            }
                                            return fitness;
                                       });

            for(int i = 0; i < 100; ++i){
                treePop.addMember(ptgen.generateRandomTree());
            }
            std::cout << "Initial population generated, contains " << treePop.getMemberCount() << " members." << std::endl;

            for(int i = 0; i < 500; ++i){
                std::cout << "Generation " << i << std::endl;
                std::cout << "Top 5:" << std::endl;
                for(int i = 0; i < 5; ++i){
                    std::cout << i << ": " << treePop.getMember(i).fitness << std::endl;
                }
                for(int i = 0; i < 100; ++i){
                    //gp::ProgramTreeNode* mem = treePop.getRandomMemberExpon().member;
                    //gp::ProgramTreeNode* tree = new gp::ProgramTreeNode(*mem);
                    //ptgen.mutateTree(*tree, 15);
                    //treePop.addMember(tree);

                    gp::ProgramTreeNode* mem1 = treePop.getRandomMemberExpon().member;
                    gp::ProgramTreeNode* mem2 = treePop.getRandomMemberExpon().member;
                    std::pair<gp::ProgramTreeNode*, gp::ProgramTreeNode*> children = ptgen.breedTrees(mem1, mem2);
                    ptgen.mutateTree(children.first, 15);
                    ptgen.mutateTree(children.second, 15);
                    treePop.addMember(children.first);
                    treePop.addMember(children.second);
                }
                treePop.cullPopulation(1000);
                //std::cin.get();
            }
            std::cout << "Best: " << std::endl;
            std::cout << ptps.debugTree(treePop.getMember(0).member) << std::endl;
            std::cout << ptps.getInfixForTree(treePop.getMember(0).member) << std::endl;

            std::cout << "Ending Quadratic program tree test..." << std::endl;
            return;
        }
    }
}

#endif // GP_QUADRATICPROGRAMTREETEST_HPP


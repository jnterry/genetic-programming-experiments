#include "MultiLayerPerceptron.hpp"
#include "DynamicMatrix.hpp"
#include <cstring>

namespace{
    gp::DynamicMatrix<float> prependBiasNeuron(gp::DynamicMatrix<float> matrix){
        gp::DynamicMatrix<float> result(1, matrix.getDimension().columns+1);
        result[0][0] = 1;
        for(int i = 0; i < matrix.getDimension().columns; ++i){
            result[0][i+1] = matrix[0][i];
        }
        return result;
    }
}

namespace gp{

        MutliLayerPerceptron::MutliLayerPerceptron(unsigned short layerCount, const unsigned short* structure)
        : mStructure(new unsigned short[layerCount]), mLayerCount(layerCount){
            //copy structure data
            memcpy(this->mStructure, structure, sizeof(unsigned short)*layerCount);

            //create the weight matricies
            for(int i = 0; i < layerCount-1; ++i){
                //layerCount -1 as we need matricies between each layer, eg if the MLP
                //has only an input and output layer (2 layers) we need 1 weight matrix
                this->mWeightMatricies.push_back(DynamicMatrix<float>(structure[i]+1, structure[i+1]+1));
            }
        }


        DynamicMatrix<float> MutliLayerPerceptron::evaluate(DynamicMatrix<float> inputs){
            if(inputs.getDimension().columns != this->getInputNeuronCount()){
                //inputs invalid
                return DynamicMatrix<float>(0,0);
            }

            DynamicMatrix<float> prevLayerResults = prependBiasNeuron(inputs);

            for(int i = 0; i < this->mWeightMatricies.size(); ++i){
                prevLayerResults = prevLayerResults * this->mWeightMatricies[i];
            }

            return prevLayerResults;
        }


        bool MutliLayerPerceptron::isValid(){
            if(this->getLayerCount() < 2){
                //must have atleast an input and output layer
                return false;
            }
            if(this->mWeightMatricies.size() != this->mLayerCount-1){
                return false;
            }
            for(int i = 0; i < this->mWeightMatricies.size(); ++i){
                //matrix 0 connects layer 0 and 1
                //layer 0 has i neurons
                //layer 1 has k neurons
                //matrix0 must convert a (1xi) matrix into a (1xk) matrix
                //ie: (1xi) * (ixk) = (1xk)
                //thus matrix0 must have the dimension layer0neuroncount x layer1NeuronCount
                //however, each layer has an additional bias nueron, so +1 to the structure data
                if(this->mWeightMatricies[i].getDimension().rows != this->mStructure[i]+1){
                    return false;
                }
                if(this->mWeightMatricies[i].getDimension().columns != this->mStructure[i+1]+1){
                    return false;
                }
            }

            //if havent returned false then must be valid
            return true;
        }

        unsigned short MutliLayerPerceptron::getInputNeuronCount(){
            return this->mStructure[0];
        }

        unsigned short MutliLayerPerceptron::getOutputNeuronCount(){
            return this->mStructure[this->mLayerCount-1];
        }

        unsigned short MutliLayerPerceptron::getHiddenLayerCount(){
            return this->mLayerCount-2;
        }

        unsigned short MutliLayerPerceptron::getLayerCount(){
            return this->mLayerCount;
        }

}
